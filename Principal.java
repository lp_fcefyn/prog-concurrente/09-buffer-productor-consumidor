// Se producir�n y consumir�n tartas, con n consumidores y m productores. Como m�ximo pueden haber 10 tartas y como m�nimo 0.
public class Principal {
	public static Object lock = new Object();
	public static int tartas = 10;
	
	public static void main(String[] args)
	{
		int n = 8;
		int m = 2;
		
		Thread c[] = new Thread[n];		// Vector consumidores
		Thread p[] = new Thread[m];		// Vector productores
		
		for(int i = 0; i <n; i++)
		{
			c[i] = new Thread(new Consumidor(i+1));
			c[i].start();
		}
		
		for(int i = 0; i <m; i++)
		{
			p[i] = new Thread(new Productor(i+1));
			p[i].start();
		}
		
		/*for(int i = 0;i < n;i++)
		{
			try {
				c[i].join();
			} catch (Exception e) {}
		}
		
		for(int i = 0;i < n;i++)
		{
			try {
				p[i].join();
			} catch (Exception e) {}
		}*/
	}
	
}

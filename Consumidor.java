
public class Consumidor implements Runnable {
	private int id;
	
	public Consumidor(int id)
	{
		this.id = id;
	}
	
	@Override
	public void run() 										
	{								
		while(true)
		{
			synchronized(Principal.lock)
			{
				if(Principal.tartas > 0)
				{
					Principal.tartas--;
					System.out.println("Soy el consumidor "+this.id+" y quedan "+Principal.tartas+" tartas");
					try {
						Thread.sleep(1000);			// Se demora al consumidor para que sea visible el proceso.
						} catch (InterruptedException e) {}
				}
				else								// Si no sos el productor, dormite y despert� a todos los hilos as� se depierta
				{									// el cocinero
					Principal.lock.notifyAll();
					try {
						Principal.lock.wait();
					} catch (InterruptedException e) {}
				}
			}
		}
	}
}

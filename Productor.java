
public class Productor implements Runnable {
	private int id;
	
	public Productor(int id)
	{
		this.id = id;
	}
	
	
	@Override
	public void run() 										
	{								
		while(true)
		{
			synchronized(Principal.lock)
			{
				if((Principal.tartas < 10) && ((Principal.tartas + 2) <= 10))
				{
					Principal.tartas += 2;
					System.out.println("Soy el productor "+this.id+" y quedan "+Principal.tartas+" tartas");
					/*try {
						Thread.sleep(1000);			// Esto si se quiere demorar al productor .
						} catch (InterruptedException e) {}*/
					Principal.lock.notifyAll();		// Ya hice m�s tartas, consumidores que se quedaron esperando (dormidos con wait)
				}									// ya pueden venir a consumir!
				/*try {
					Principal.lock.wait();					// Si en vez de sumar de a 2, se llenara el buffer con 10 tartas de una
					} catch (InterruptedException e) {}*/	// y si en vez de cocinar cuando hay menos de 10 tartas se concinara cuando
			}												// no quedan m�s tartas, se utilizar�a esto: Repongo las 10 tartas, llamenm� de 
		}													// nuevo cuando se acaben.
	}
}
